module.exports = {
    env: {
      es6: true,
      node: true,
      'react-native/react-native': true,
    },
    extends: [
      'eslint:recommended',
      'plugin:react/recommended',
      'plugin:react-native/all',
      'plugin:@typescript-eslint/recommended',  // Si estás usando TypeScript
      'prettier', // Integración con Prettier para formateo automático
    ],
    parserOptions: {
      ecmaFeatures: {
        jsx: true,
      },
      ecmaVersion: 2021,
      sourceType: 'module',
    },
    plugins: [
      'react',
      'react-native',
      '@typescript-eslint', // Si usas TypeScript
    ],
    rules: {
      'react/prop-types': 'off', // Si no estás usando PropTypes
      'react/react-in-jsx-scope': 'off', // Con React 17+ ya no es necesario importar React en cada archivo
      'react-native/no-inline-styles': 'warn', // Evitar estilos en línea
      'no-unused-vars': 'warn', // Avisar sobre variables no usadas
      'no-console': 'warn', // Avisar si se usa console.log
    },
    settings: {
      react: {
        version: 'detect', // Detectar la versión de React automáticamente
      },
    },
  };
  