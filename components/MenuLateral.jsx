import React from "react";
import { View, StyleSheet, Alert } from "react-native";
import { Drawer, Text, IconButton } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

const MenuLateral = ({ onLogout }) => {
  const navigation = useNavigation();

  const handleLogout = () => {
    Alert.alert(
      "Cerrar Sesión",
      "¿Estás seguro de que quieres cerrar sesión?",
      [
        {
          text: "Cancelar",
          style: "cancel",
        },
        {
          text: "Cerrar Sesión",
          onPress: () => {
            onLogout();
            navigation.navigate("Login");
          },
        },
      ]
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text variant="titleLarge">Menú</Text>
        <IconButton
          icon="close"
          size={24}
          onPress={() => navigation.closeDrawer()}
        />
      </View>
      <Drawer.Section title="Opciones">
        <Drawer.Item
          label="Perfil"
          onPress={() => {
            navigation.navigate("Profile");
            navigation.closeDrawer(); // Cerrar el drawer al navegar
          }}
        />
        <Drawer.Item 
          label="Cerrar Sesión" 
          onPress={handleLogout} 
        />
      </Drawer.Section>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: "#fff",
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default MenuLateral;
