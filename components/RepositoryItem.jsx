import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

const RepositoryItem = ({
  fullName,
  description,
  language,
  forksCount,
  stargazersCount,
  ratingAverage,
  reviewCount,
  ownerAvatarUrl,
}) => {
  return (
    <View style={styles.container}>
      <Image style={styles.avatar} source={{ uri: ownerAvatarUrl }} />
      <View style={styles.infoContainer}>
        <Text style={styles.fullName}>{fullName}</Text>
        <Text style={styles.description}>{description}</Text>
        <Text style={styles.language}>{language}</Text>
      </View>
      <View style={styles.statsContainer}>
        <Text>{forksCount} Forks</Text>
        <Text>{stargazersCount} Stars</Text>
        <Text>{ratingAverage} Rating</Text>
        <Text>{reviewCount} Reviews</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
    backgroundColor: 'white',
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 5,
  },
  infoContainer: {
    flexDirection: 'column',
    paddingLeft: 10,
  },
  fullName: {
    fontWeight: 'bold',
  },
  description: {
    color: 'gray',
  },
  language: {
    paddingVertical: 5,
    paddingHorizontal: 10,
    backgroundColor: '#0366d6',
    color: 'white',
    alignSelf: 'flex-start',
    borderRadius: 5,
  },
  statsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: 10,
  },
});

export default RepositoryItem;
