import React from 'react';
import { FlatList,StyleSheet, View,Text } from 'react-native';
import CategoryItem from './CategoryItem';
import Separator from './Separator';
import { SafeAreaView } from 'react-native-safe-area-context';

const categories = [
  { title: 'Modista', icon: 'puzzle-edit' },
  { title: 'Planchadores', icon: 'iron' },
  { title: 'Cortador', icon: 'content-cut' },
  { title: 'Tallerista', icon: 'fence' },
  { title: 'Encimador', icon: 'layers' },
  { title: 'Moldista', icon: 'shape' },
  { title: 'Alta costura', icon: 'ruler-square-compass' },
  { title: 'Taller de punto', icon: 'credit-card-settings' },
  { title: 'Tizador', icon: 'nut' },
  { title: 'Muestrista', icon: 'hanger' },
];

const HeaderComponent = () => (
  <View style={styles.headerContainer}>
    <Text style={styles.headerTitle}>Categorías</Text>
  </View>
);

const CategoryList = () => (
  <SafeAreaView style = {{ padding: 5,}}>
    <FlatList
      data={ categories }
      renderItem={({ item }) => (
        <CategoryItem title={item.title} icon={item.icon} />
      )}
      keyExtractor={(item, index) => index.toString()}
      ItemSeparatorComponent={() => <Separator />}
      contentContainerStyle={styles.container}
      ListHeaderComponent={<HeaderComponent />}
    />
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
    padding: 5
  },
  headerContainer: {
    marginBottom: 20,
    padding: 20,
    backgroundColor: '#6200ea',
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
  },
});

export default CategoryList;  