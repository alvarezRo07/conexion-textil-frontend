import React, { useState } from 'react';
import { View, StyleSheet, Alert, Image } from 'react-native';
import { Text, TextInput, Button, Card, Title, Provider as PaperProvider } from 'react-native-paper';
import axios from 'axios'; 

const LoginScreen = ({ onLogin }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async () => {
    try {
      const response = await axios.post('/api/login', {
        email: email,
        password: password,
      });

      if (response.data.success) {
        Alert.alert('Login Successful');
        onLogin();
      } else {
        Alert.alert('Login Failed', 'Incorrect email or password');
      }
    } catch (error) {
      Alert.alert('Login Failed', 'An error occurred');
      console.error(error);
    }
  };

  const [titleText, setTitleText] = useState("Conexión Textil");

  return (
    <PaperProvider>
      <View style={styles.container}>
        <Image 
          source={require('../assets/knitting.png')}
          style={styles.icon} />
        <Text style={styles.titleText}>
          {titleText}
          {'\n'}
          {'\n'}
        </Text>
        <Card style={styles.card}>
          <Card.Content>
            <Title style={styles.title}></Title>
            <TextInput
              label="Email"
              value={email}
              onChangeText={setEmail}
              style={styles.input}
              mode="outlined"
            />
            <TextInput
              label="Password"
              value={password}
              onChangeText={setPassword}
              style={styles.input}
              mode="outlined"
              secureTextEntry
            />
            <Button 
              mode="contained"
              onPress={onLogin} 
              //onPress={handleLogin} 
              style={styles.button}
              textColor='#FFF'
              buttonColor='#002EE8'
             >
              Aceptar
            </Button>
          </Card.Content>
        </Card>
      </View>
    </PaperProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    backgroundColor: '#f5f5f5',
  },
  icon: {
    width: 100,
    height: 100,
    marginBottom: 20,
  },
  titleText: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'blue',
    textAlign: 'center',
    marginBottom: 20,
  },
  card: {
    width: '80%',
    padding: 20,
    borderRadius: 10,
    shadowColor: '#000',
    backgroundColor:'#C0C5D2',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  title: {
    fontSize: 28,
    color:'#002EE8',
    marginBottom: 16,
    textAlign: 'center',
  },
  input: {
    backgroundColor: '#FFFFFF',
    marginBottom: 12,
  },
  button: {
    marginTop: 12,
  },
});

export default LoginScreen;
