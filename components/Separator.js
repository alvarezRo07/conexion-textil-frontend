import React from 'react';
import { View, StyleSheet } from 'react-native';

const Separator = () => {
  return <View style={styles.separator} />;
};

const styles = StyleSheet.create({
  separator: {
    height: 1, // Define el grosor de la línea
    width: '100%', // Ocupa todo el ancho del contenedor
    backgroundColor: '#ccc', // Color gris claro para la línea
    marginVertical: 10, // Espaciado vertical alrededor del separador
  },
});

export default Separator;