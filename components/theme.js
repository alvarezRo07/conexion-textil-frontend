const theme = {
    colors: {
      primary: '#0366d6',
      secondary: '#ff6347',
      textPrimary: '#24292e',
      textSecondary: '#586069',
      white: '#ffffff',
      backgroundLight: '#f5f5f5',
      error: '#d73a4a',
    },
    fontSizes: {
      body: 14,
      subheading: 16,
      title: 20,
    },
    fonts: {
      main: 'System', // En iOS usa "System", en Android "Roboto" o cualquier fuente que elijas
    },
    fontWeights: {
      normal: '400',
      bold: '700',
    },
    spacing: {
      small: 8,
      medium: 16,
      large: 24,
    },
    borders: {
      radius: 4,
    },
  };
  
  export default theme;
  