import React from 'react';
import { View, StyleSheet } from 'react-native';
import StyledText from './StyledText';
import theme from './theme';

// Función para formatear los números grandes, como 1.5k en vez de 1500
const parseThousands = value => {
  return value >= 1000 ? `${(value / 1000).toFixed(1)}k` : String(value);
};

const RepositoryStats = ({ stargazersCount, forksCount, reviewCount, ratingAverage }) => {
  return (
    <View style={styles.container}>
      <View style={styles.statItem}>
        <StyledText align="center" fontWeight="bold">{parseThousands(stargazersCount)}</StyledText>
        <StyledText align="center">Stars</StyledText>
      </View>
      <View style={styles.statItem}>
        <StyledText align="center" fontWeight="bold">{parseThousands(forksCount)}</StyledText>
        <StyledText align="center">Forks</StyledText>
      </View>
      <View style={styles.statItem}>
        <StyledText align="center" fontWeight="bold">{reviewCount}</StyledText>
        <StyledText align="center">Reviews</StyledText>
      </View>
      <View style={styles.statItem}>
        <StyledText align="center" fontWeight="bold">{ratingAverage}</StyledText>
        <StyledText align="center">Rating</StyledText>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: theme.spacing.medium,
  },
  statItem: {
    alignItems: 'center',
  },
});

export default RepositoryStats;