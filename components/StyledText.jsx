import React from 'react';
import { Text, StyleSheet } from 'react-native';
import theme from './theme';

export default function StyledText({ children, color, fontSize, fontWeight, style, ...rest }) {
  const textStyles = [
    styles.text,
    color === 'primary' && styles.primary,
    color === 'secondary' && styles.secondary,
    fontSize === 'subheading' && styles.subheading,
    fontWeight === 'bold' && styles.bold,
    style, // permite sobrescribir los estilos si es necesario
  ];

  return (
    <Text style={textStyles} {...rest}>
      {children}
    </Text>
  );
}

const styles = StyleSheet.create({
  text: {
    fontFamily: theme.fonts.main,
    fontSize: theme.fontSizes.body,
    color: theme.colors.textPrimary,
    fontWeight: theme.fontWeights.normal,
  },
  primary: {
    color: theme.colors.primary,
  },
  secondary: {
    color: theme.colors.secondary,
  },
  subheading: {
    fontSize: theme.fontSizes.subheading,
  },
  bold: {
    fontWeight: theme.fontWeights.bold,
  },
});
