import React from 'react';
import { View, StyleSheet, TouchableWithoutFeedback } from 'react-native';
import Constants from 'expo-constants';
import StyledText from './StyledText';
import theme from './theme';
import { Link, useLocation } from 'react-router-native';

const AppBarTab = ({ children, to }) => {
  const { pathname } = useLocation();
  const active = pathname === to;

  const textStyles = [
    styles.text,
    active && styles.active,
  ];

  return (
    <Link to={to} component={TouchableWithoutFeedback}>
      <StyledText fontWeight="bold" style={textStyles}>
        {children}
      </StyledText>
    </Link>
  );
};

const AppBar = () => {
  return (
    <View style={styles.container}>
      <AppBarTab to="/">Repositories</AppBarTab>
      <AppBarTab to="/signin">Sign In</AppBarTab>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: Constants.statusBarHeight + theme.spacing.small,
    paddingBottom: theme.spacing.small,
    backgroundColor: theme.colors.primary,
  },
  text: {
    color: theme.colors.white,
    padding: theme.spacing.small,
  },
  active: {
    color: theme.colors.secondary,
  },
});

export default AppBar;