import React from "react";
import { StyleSheet } from "react-native";
import { List, Avatar } from "react-native-paper";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const CategoryItem = ({ title, icon }) => (
  <List.Item
    style = {styles.container}
    title={title}
    textStyle={styles.textTitle}
    left={() => (
      <Avatar.Icon 
        size={42}
        icon={() => 
          <Icon 
            name={icon} 
            size={35} 
            color="#FFFF" />} 
      />
    )}
    onPress={() => console.log(`${title} pressed`)}
  />
);

const styles = StyleSheet.create({
  container: { 
    backgroundColor: "#f9f5f9",
    borderRadius: 10,
    marginBottom: 10,
    padding:10 
  },
  textTitle: {
    color: "#000",
    fontSize: 20,
    marginLeft:10,
    flex: 1,
    justifyContent: "center",
  },
});

export default CategoryItem;
