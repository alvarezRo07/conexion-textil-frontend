import React, { useState } from "react";
import { StyleSheet } from "react-native";
import { Provider as PaperProvider, IconButton } from "react-native-paper";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";

//Components
import LoginScreen from "./components/LoginScreen";
import CategoryList from "./components/CategoryList";
import RepositoryItem from "./components/RepositoryItem";
import MenuLateral from "./components/MenuLateral";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const MainStack = ({ navigation }) => {
  return (
    <Stack.Navigator>
      <Stack.Screen 
        name="Categories" 
        component={CategoryList} 
        options={{
          title: "Categorías",
          headerLeft: () => (
            <IconButton
              icon="menu"
              size={24}
              onPress={() => navigation.toggleDrawer()} // Abrir menú lateral con animación
            />
          ),
        }} 
      />
      <Stack.Screen 
        name="Profile" 
        component={RepositoryItem} 
        options={{
          title: "Perfil",
        }}
      />
    </Stack.Navigator>
  );
};

const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const handleLogin = () => {
    setIsLoggedIn(true);
  };

  const handleLogout = () => {
    setIsLoggedIn(false);
  };
  
  return (
    <PaperProvider>
      <NavigationContainer>
      {isLoggedIn ? (
          <Drawer.Navigator
            screenOptions={{
              drawerType: 'slide',
              swipeEnabled: true,
            }}
            drawerContent={(props) => (
              <MenuLateral {...props} onLogout={handleLogout} />
            )}
          >
            <Drawer.Screen 
              name="MainStack" 
              component={MainStack} 
              options={{ headerShown: false }} // Ocultar el header del Drawer
            />
          </Drawer.Navigator> 
          ) : (
          <Stack.Navigator>
            <Stack.Screen name="Login">
              {(props) => <LoginScreen {...props} onLogin={handleLogin} />}
            </Stack.Screen>
          </Stack.Navigator>
        )}
      </NavigationContainer>
    </PaperProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },

  logoutButtonContainer: {
    padding: 16,
    backgroundColor: "#f5f5f5",
    alignItems: "center",
  },
  button: {
    marginTop: 16,
  },
});

export default App;
